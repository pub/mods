mods was originally written by Neil Gershenfeld (https://ng.cba.mit.edu/) for rapid-prototyping workflows in CBA research (http://cba.mit.edu) and classes (https://fab.cba.mit.edu/classes/MAS.863/), and for use in the fab lab network (https://fablabs.io/labs/map) and its classes(https://academany.org/).

It is now served (https://modsproject.org) and supported (https://gitlab.fabcloud.org/pub/project/mods) as a community project. https://mods.cba.mit.edu now redirects to here; original mods remains available at https://mods.cba.mit.edu/oldindex.html
